FactoryBot.define do
  factory :user do
    sequence(:email) { |n| "user#{n}@example.com" }
    name { Faker::Name.name_with_middle }
    position { Faker::Job.position }
  end
end
