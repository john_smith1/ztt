require 'rails_helper'

RSpec.describe 'Users', type: :request do
  describe 'GET /users.json' do
    let!(:users) { create_list :user, 10 }

    it 'fetches users list' do
      get users_path
      expect(response.status).to eq(200)
      fetched_ids = JSON.parse(response.body)['data'].map { |u| u['id'].to_i }
      expect(fetched_ids).to eq(users.map(&:id))
    end
  end
end
