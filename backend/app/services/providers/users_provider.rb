module Providers
  class UsersProvider
    def all
      # We can control what users are available through API here
      User.all
    end
  end
end
