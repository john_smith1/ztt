class UserResource < JSONAPI::Resource
  attributes :email, :name, :position

  def self.records(_ctx = {})
    Providers::UsersProvider.new.all
  end
end
