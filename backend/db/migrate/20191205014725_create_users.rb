class CreateUsers < ActiveRecord::Migration[6.0]
  def change
    create_table :users do |t|
      t.string :email, unique: true, null: false
      t.string :name, null: false
      t.string :position, null: true

      t.timestamps
    end
  end
end
