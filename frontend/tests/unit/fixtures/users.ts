import { IUser } from "../../../src/services/api/apiTypes";

export const fixtureUsers = (): IUser[] => [
  { id: "1", email: "old1@example.com", name: "John Doe" },
  {
    id: "2",
    email: "old2@example.com",
    name: "Tim Peters",
    position: "CEO"
  }
];
