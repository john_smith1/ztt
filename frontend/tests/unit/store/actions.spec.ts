import { IApiService } from "@/services/api/apiTypes";
import { fixtureUsers } from "../fixtures/users";
import { createActions } from "@/store/actions";

const apiMock: IApiService = {
  async getUsers(page, pageSize = 2) {
    return [fixtureUsers(), 10];
  }
};

const actions = createActions(apiMock);

describe("fetchUsers action", () => {
  it("wraps updateUsers action with toggling loading on and off", async () => {
    const commit = jest.fn();
    const dispatch = jest.fn();

    // @ts-ignore
    await actions.fetchUsers({ commit, dispatch });

    expect(commit).toBeCalledWith("SET_LOADING", true);
    expect(dispatch).toBeCalledWith("updateUsers");
    expect(commit).toBeCalledWith("SET_LOADING", false);
  });
});

describe("updateUsers action", () => {
  pending("calls API to fetch users");
});
describe("selectPage action", () => {
  pending("changes page in state and triggers updateUsers to load users");
});
