import { mutations } from "@/store/mutations";
import { IPagination, IState } from "@/store/storeTypes";
import { defaultState } from "@/store/defaultState";
import { IUser } from "@/services/api/apiTypes";
import { fixtureUsers } from "../fixtures/users";

describe("SET_USERS", () => {
  it("replaces users in state with passed response", () => {
    const oldUsers = fixtureUsers();

    const state: IState = { ...defaultState(), users: oldUsers };

    const newUsers: IUser[] = [
      {
        id: "3",
        email: "other@example.com",
        name: "Alex Smith",
        position: "Other"
      }
    ];

    mutations.SET_USERS(state, newUsers);

    expect(state.users).toEqual(newUsers);
  });
});

describe("SET_PAGE_COUNT", () => {
  it("replaces pagination.pageCount", () => {
    const state: IState = defaultState();

    const newPageCount = 5;

    mutations.SET_PAGE_COUNT(state, newPageCount);

    expect(state.pagination.pageCount).toEqual(newPageCount);
  });

  it("adjusts pagination.page to new pageCount", () => {
    const state: IState = {
      ...defaultState(),
      pagination: {
        page: 10,
        pageCount: 15,
        pageSize: 10
      }
    };

    const newPageCount = 5;

    mutations.SET_PAGE_COUNT(state, newPageCount);

    expect(state.pagination.page).toEqual(newPageCount);
  });
});

describe("SET_PAGE", () => {
  const createPagination = (page: number, pageCount: number): IPagination => ({
    page,
    pageCount,
    pageSize: 10
  });

  it("sets pagination.page", () => {
    const state: IState = {
      ...defaultState(),
      pagination: createPagination(10, 20)
    };
    const newPage = 2;

    mutations.SET_PAGE(state, newPage);

    expect(state.pagination.page).toEqual(newPage);
  });

  it("adjusts page if value is below zero", () => {
    const state: IState = {
      ...defaultState(),
      pagination: createPagination(10, 20)
    };
    const newPage = -1;

    mutations.SET_PAGE(state, newPage);

    expect(state.pagination.page).toEqual(1);
  });

  it("adjusts page if value is over available pages number", () => {
    const state: IState = {
      ...defaultState(),
      pagination: createPagination(10, 20)
    };
    const newPage = 50;

    mutations.SET_PAGE(state, newPage);

    expect(state.pagination.page).toEqual(20);
  });
});
