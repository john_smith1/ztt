import { MutationTree } from "vuex";
import { IState } from "@/store/storeTypes";
import { IUser, IUsersResponse } from "@/services/api/apiTypes";

export const mutations: MutationTree<IState> = {
  SET_USERS(state, users: IUser[]) {
    state.users = users;
  },
  SET_PAGE_COUNT(state, pageCount) {
    state.pagination.pageCount = pageCount;

    if (state.pagination.page > pageCount) {
      state.pagination.page = pageCount;
    }
  },
  SET_LOADING(state: IState, loading: boolean) {
    state.loading = loading;
  },
  SET_PAGE(state: IState, page: number) {
    if (page < 0) {
      page = 1;
    }

    if (page > state.pagination.pageCount) {
      page = state.pagination.pageCount;
    }

    state.pagination.page = page;
  }
};
