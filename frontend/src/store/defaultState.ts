export const defaultState = () => ({
  users: [],
  pagination: { page: 1, pageSize: 10, pageCount: 1 },
  loading: true
});
