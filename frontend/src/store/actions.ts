import { IApiService, IUsersResponse } from "@/services/api/apiTypes";
import { ActionTree } from "vuex";
import { IState } from "@/store/storeTypes";

export const createActions = (
  api: IApiService
): ActionTree<IState, IState> => ({
  async fetchUsers({ commit, dispatch }) {
    commit("SET_LOADING", true);
    await dispatch("updateUsers");
    commit("SET_LOADING", false);
  },
  async updateUsers({ state, commit }) {
    const {
      pagination: { page, pageSize }
    } = state;

    const [users, pageCount] = await api.getUsers(page, pageSize);

    // If we had more api calls we could figure out if it makes sense to
    // just have single mutation do this
    commit("SET_USERS", users);
    commit("SET_PAGE_COUNT", pageCount);
  },
  async selectPage({ commit, dispatch }, page: number) {
    commit("SET_PAGE", page);
    await dispatch("updateUsers");
  }
});
