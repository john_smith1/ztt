import { IUser } from "@/services/api/apiTypes";

export interface IPagination {
  page: number;
  pageCount: number;
  pageSize: number;
}

export interface IState {
  users: IUser[];
  pagination: IPagination;
  loading: boolean;
}
