import Vue from "vue";
import Vuex from "vuex";
import { IState } from "./storeTypes";
import { mutations } from "./mutations";
import { createActions } from "./actions";
import { defaultState } from "./defaultState";
import { apiService } from "@/services/api";

Vue.use(Vuex);

export default new Vuex.Store<IState>({
  state: defaultState(),
  mutations,
  actions: createActions(apiService),
  modules: {}
});
