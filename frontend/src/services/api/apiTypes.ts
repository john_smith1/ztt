type GetUsersResponse = [Array<IUser>, number];

export interface IApiService {
  getUsers(page: number, pageSize?: number): Promise<GetUsersResponse>;
}

export interface IUser {
  id: string;
  email: string;
  name: string;
  position?: string;
}

export interface IResourcesMeta {
  "page-count": number;
}

interface IUserData {
  id: IUser["id"];
  attributes: Omit<IUser, "id">;
}

export interface IUsersResponse {
  data: IUserData[];
  meta: IResourcesMeta;
}
