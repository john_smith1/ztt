import axios from "axios";
import { IApiService, IUsersResponse } from "@/services/api/apiTypes";

const api = axios.create({
  baseURL: "http://localhost:3000"
});

export const apiService: IApiService = {
  async getUsers(page, pageSize = 10) {
    const { data: response } = await api.get<IUsersResponse>("users.json", {
      params: { "page[number]": page, "page[size]": pageSize }
    });

    const { data, meta } = response;

    // Naive deserialization because it's just one API call
    const users = data.map(({ id, attributes: { email, name, position } }) => ({
      id,
      email,
      name,
      position
    }));

    const pageCount = meta["page-count"];

    return [users, pageCount];
  }
};
