# Users List Test Task

## Description

Build a small webapp that allows to display and paginate through User model with some dummy fields

## Stack

__Backend:__ Ruby 2.6, Rails 6.0 in API-only configuration, mailers and ActionCable stripped down, Postgresql database
__Fronend:__ Vue.js 2.6, Typescript, without router for simplicity, but with Vuex

## Implementation details

__API__ is JSONAPI spec 1.0, pagination mode is pageSize/page, other option would be limit/offset, but IMO it's a matter of taste in most cases (continuous scrolling is better with limit/offset though).
On backend jsonapi-resources gem is used, on frontend it's just plain Axios, but in prod I would wrap it with something that supports all jsonapi features.

__Frontend Routing__ is skipped to make rest of code easier to assess, it means that frontend doesn't persist pagination. It can be a follow-up task if company is interested, but it's quite trivial matter:
adding router, adding hooks between router and store to mutually update, populate default state on page load from URL.

## Tests

On backend there is couple Rspec tests: model and request spec for users list

On frontend store logic is tested

## Installation

__Frontend__ is in `/frontend` directory and is a vue-cli app, so all you have to do is `yarn install` and then use `yarn serve` to run app and `yarn test:unit` to run tests.

__Backend__ is normal rails app. You need to have a Postgresql server running on your machine and have Ruby 2.6 installed (`.ruby-version` file is provided for your RVM/Rbenv). 
Run `bundle` to install dependencies, `rake db:setup` to create DB and test data, and then `rails s` for server and `rake` for tests.
